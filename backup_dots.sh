#PATHS
PATH_ZSHRC='/home/tim/.zshrc'
PATH_VIMRC='/home/tim/.vimrc'
PATH_I3CONFIG='/home/tim/.i3/config'
PATH_I3_FOLDER='/home/tim/.config/i3/'
PATH_I3STATUS='/home/tim/.config/i3status/i3status.conf'
PATH_XRES='/home/tim/.Xresources'
PATH_BACKUPS='/home/tim/dotfiles/'
PATH_COMPTON='/home/tim/.config/compton/compton.conf'

#command
COMMAND='cp'
ARGS='-r -v'

#readout
echo 'Copying Dots'

#COPY
$COMMAND $ARGS $PATH_ZSHRC $PATH_BACKUPS
$COMMAND $ARGS $PATH_VIMRC $PATH_BACKUPS
$COMMAND $ARGS $PATH_I3CONFIG $PATH_BACKUPS
$COMMAND $ARGS $PATH_I3_FOLDER $PATH_BACKUPS
$COMMAND $ARGS $PATH_I3STATUS $PATH_BACKUPS
$COMMAND $ARGS $PATH_XRES $PATH_BACKUPS
$COMMAND $ARGS $PATH_COMPTON $PATH_BACKUPS 

