function click(tab)
{
  chrome.tabs.query({currentWindow:true, active:true}, function(tabs)
  {
    getSettings(function(items)
    {
      if(!items.deactivated)
        chrome.tabs.sendMessage(tabs[0].id, { action: "toggle_dark_mode" });
    });
  });

  chrome.pageAction.onClicked.addListener(click);

  chrome.runtime.onInstalled.addListener(function (o) {
  	chrome.declarativeContent.onPageChanged.removeRules(['googleRule'], function () {
  		chrome.declarativeContent.onPageChanged.addRules([{
  			id: 'googleRule',
  			conditions: [
  				new chrome.declarativeContent.PageStateMatcher({
  					//pageUrl: { urlContains: 'google'}
            pageUrl: { urlMatches: '(google|darkmode.browserextension)\.' }
  				})
  			],
  			actions: [
  				new chrome.declarativeContent.ShowPageAction()
  			]
  		}]);
  	});
  		// insert your landing page here
  		//	    chrome.tabs.create({url: "https://formybrowser.com/dark-mode-for-google/?installed=thankyou&browser=chrome"}, function (tab) {
  		//        console.log("New tab launched with https://formybrowser.com/dark-mode-for-google/?installed=thankyou&browser=chrome");
  		//    });
  });

  function getSettings(callback)
  {
    chrome.storage.sync.get({
      deactivated: false
    }, function(items) {
      if (callback) {
        callback(items);
      }
    });
  }
}
