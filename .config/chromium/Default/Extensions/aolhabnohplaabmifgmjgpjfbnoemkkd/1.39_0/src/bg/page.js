chrome.runtime.onMessage.addListener(function(msg, sender, sendResponse) {

	if (msg.action == "toggle_dark_mode") {

		getSettings(function(items) {

			if (items.darkModeOn) {

				addDarkMode();

			} else {

				$('#google_dark_mode_link').remove();
			}
		});
	}

});

getSettings(function(items) {

	// alert(items.darkModeOn);

	if (!items.deactivated) {
		addDarkMode();
	} 

	chrome.runtime.sendMessage({action: "update_button", darkMode:items.darkModeOn}, function(response) {

		
	});
});



function getSettings(callback) {

	chrome.storage.sync.get({

		deactivated: false,

	}, function(items) {

		if (callback) {

			callback(items);
		}
	});  
}

function addDarkMode() {
var nothing = 0;

	var searcher = ["docs.google",
					"accounts.google", 
					"books.google\..+\/books", 
					"blogger\..+", 
					"groups.google", 
					"calendar.google", 
					"translate.google", 
					//"scholar.google", 
					"news.google", 
					"mail.google", 
					"maps.google", 
					"/analytics/", 
					"/adwords/", 
					"google\..+\/trends", 
					"google\..+\/admob", 
					"google\..+\/business", 
					"google\..+\/adsense", 
					"google\..+\/flights", 
					"google\..+\/maps", 
					"google\..+\/keep", 
					"apps\.google\..+", 
					"developers\.google\..+", 
					"plus\.google\..+", 
					"store\.google\..+", 
					"google\..+\/drive", 
					"google\..+\/cloudprint", 
					"google\..+\/intl\/.+\/picasa", 
					"google\..+\/intl\/.+\/drive", 
					"google\..+\/intl\/.+\/docs", 
					"google\..+\/intl\/.+\/sheets", 
					"google\..+\/intl\/.+\/slides",
					"google\..+\/intl\/.+\/forms"];

	for(var x=0; x<searcher.length; x++){
		if(document.URL.match(searcher[x]) != null){
			nothing++;
		}
	}

	//console.log(nothing);
	if(nothing < 1){
		var link = document.createElement('link');
		link.href =  chrome.extension.getURL('src/inject/inject_active.css');
		link.rel = 'stylesheet';
		link.id = 'google_dark_mode_link';
		head = document.getElementsByTagName ("head")[0] || document.documentElement;
		head.insertBefore(link, head.firstChild);
	}
}