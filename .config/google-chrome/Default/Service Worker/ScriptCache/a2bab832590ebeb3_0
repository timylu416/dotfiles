0\r�m��      ki��    954/**
 * User: Hussein Qudsi
 * Date: 3/04/2016
 * Time: 17:11
 */
/* global twc */
/*jshint -W065 */
/*Version 0.1 */
(function () {
  'use strict';

  /** 1. vars */
  var pushFallBack = {
    title: "TWC Alerts",
    body: "The Weather Channel breaking news & sever alerts near your location, Read more",
    icon: '/images/touch/icon-128x128.png',
    tag: "TWC Push Notifications",
    data: {from: 'pushFallBack'},
    urlToOpen: "//weather.com"
  };

  var swHost = self.location.host, prodDSXHosts = ['weather.com', 'drupal-stage-web.weather.com', 'astg.weather.com'];
  var DSX_NOTIFICATION_QUEUE_TEST = 'https://' + swHost + '/js/SampleDSXNotificationQ.js'; // TESTING LOCAL DATA
  var DSX_NOTIFICATION_QUEUE = 'https://' + ((prodDSXHosts.indexOf(swHost) !== -1) ? 'dsx' : 'dsx-stage') + '.weather.com/gcm/messages/',
    baseUrl = 'https://' + swHost, twcLogo = 'https://s.w-x.co/TWC_logo_100x100.gif',
    Events = {}, PushEvent = {}; // Events & PushEvent are namespaces that'll be extended w/ methods

  /**
   * 2. Helper Fnc
   * All work done in the controller goes here in the form of well-defined functions
   */
  var _helperFnc = {
    /**
     * returnPromise
     * Function that returns a promise
     * callBack = a function, it'll invoke the callback w/ two parameters: data and resolve
     */
    returnPromise: function (callBack) {
      return function () {
        var arg = arguments;
        return new Promise(function (resolve, reject) {
          arg[arg.length++] = resolve;
          arg[arg.length++] = reject;
          callBack.apply(_helperFnc, arg);
        });
      };
    },

    /**
     * Extending helper fnc
     * @param toObj = obj that'll contain new props
     * @param fromObj = the from obj
     * @param fnc = add events handlers
     * */
    extend: function (toObj, fromObj, fnc) {
      this.forEach(fromObj, function (data, key) {
        !fnc ? toObj[key] = fromObj[key] : toObj[fnc](key, Events[key]);
      });
    },

    /**
     * forEach iteration helper
     * @param data = the iterating
     * @param callBack = callBack fnc
     * @param context = 'this' keyword
     * **/
    forEach: function (data, callBack, context) {
      if (!data) { // If falsy, existing
        return;
      }
      var i;
      if (Array.isArray(data) || data.length === +data.length && (data.length - 1) in data) { // If array or array like:
        for (i = 0; i < data.length; i++) {
          callBack.call(context || data[i], data[i], i, data);
        }
      } else {
        if (typeof data === 'object') { // If object
          for (i in data) {
            data.hasOwnProperty(i) && callBack.call(context || data[i], data[i], i, data);
          }
        }
      }
    },

    /**
     * pushResponse
     * Factory function that iteration for push notifications and return a obj containing the notification content
     * @param data = the iterating data
     * @param i = the index
     * **/
    pushResponse: function (data, i) {
      if (data.content) {
        // pushResponse vars
        var pageLocale, todayUrlPath, showNotification = true, notificationData = {}, urlToOpen;// Notification vars
        var dsxObj = JSON.parse(data.content), productType = dsxObj.product, expiryTime = data.expires, isLongMillis = false;// Data vars
        switch (productType) {
          case 'severe':
            expiryTime = dsxObj.expirationTime;
            notificationData.title = dsxObj.description;
            notificationData.body = dsxObj.title;
            pageLocale = _helperFnc.sanitizeLocale(dsxObj.language);
            var alertUrlPath = _helperFnc.getLocaleData(pageLocale, "alert_details");
            todayUrlPath = _helperFnc.getLocaleData(pageLocale, "today");
            notificationData.icon = twcLogo;
            notificationData.tag = productType + "" + i;
            //Build the Landing page URL
            var lat = dsxObj.lat && dsxObj.lat.toFixed(2), lon = dsxObj.lon && dsxObj.lon.toFixed(2), phenomena = dsxObj.phenom, significance = dsxObj.significance, areaid = dsxObj.location, office = dsxObj.officeId, etn = dsxObj.etn;
            if (lat && lon)
              if (phenomena && significance && areaid && office && etn) {
                //Alert details page
                urlToOpen = baseUrl + alertUrlPath + lat + "," + lon + "?phenomena=" + phenomena + "&significance=" + significance + "&areaid=" + areaid + "&office=" + office + "&etn=" + etn + "&from=launch:alert:pushNotification:severe";
              } else {
                //Today page
                urlToOpen = baseUrl + todayUrlPath + lat + "," + lon + "?from=launch:alert:pushNotification:severe";
              }
            break;
          case 'global8':
            expiryTime = dsxObj.g8eventEndGmt;
            pageLocale = _helperFnc.sanitizeLocale(dsxObj.language), lat = dsxObj.g8lat, lon = dsxObj.g8lon;
            notificationData.title = _helperFnc.getLocaleData(pageLocale, "g8Title");
            notificationData.body = dsxObj.localizedText;
            notificationData.icon = twcLogo;
            notificationData.tag = (productType) + "" + i;
            todayUrlPath = _helperFnc.getLocaleData(pageLocale, "today");
            if (lat && lon) {
              urlToOpen = baseUrl + todayUrlPath + lat + "," + lon;
            }
            urlToOpen += "?from=launch:alert:pushNotification:global8";
            break;
          case 'breakingnews':
            isLongMillis = true;
            notificationData.title = dsxObj.headline;
            notificationData.body = dsxObj.title;
            notificationData.icon = twcLogo;//dsxObj.imgSrc;
            notificationData.tag = (productType) + "" + i;
            urlToOpen = dsxObj.articleUrl + "?from=launch:alert:pushNotification:breaking_news";
            break;
          default :
            // Do not show any notification
            showNotification = false;
        }
        notificationData.urlData = {
          url: urlToOpen
        };
        // Sending notification
        if (showNotification) {
          if (_helperFnc.isAlertValid(expiryTime, isLongMillis)) {
            _helperFnc.primeCache(urlToOpen);
            return notificationData;
          }
        }
      }
    },

    primeCache: function (url) {
      fetch(url, {
        method: 'GET',
        mode: 'no-cors'
      }).then(function (response) {
          return response;
        }).then(function(myBlob) {
          //do nothing
        });
    },

    /**
     * isAlertValid - Checks if the alert is still valid based on expiration time
     * @param expiryTime = the expiryTime of the alert
     **/
    isAlertValid: function (expiryTime, isLongMillis) {
      if(expiryTime) {
        var now = new Date(), alertExpiryDate;
        if (isLongMillis) {
          alertExpiryDate = new Date(expiryTime);
        } else {
          expiryTime = "" + expiryTime;
          if (expiryTime.length === 12) {
            var year = expiryTime.substring(0,4), month = expiryTime.substring(4,6), day = expiryTime.substring(6,8), hour = expiryTime.substring(8,10), min = expiryTime.substring(10);
            alertExpiryDate = new Date(Date.UTC(year, month - 1, day, hour || 0, min || 0, 0, 0));
          }
        }
        return (alertExpiryDate > now);
      }
      return true;
    },

    /**
     * getLocaleData
     * @param locale = the locale to get the url
     * @param key = the key
     **/
    getLocaleData: function (locale, key) {
      var localeData = _helperFnc.getLocaleMap()[locale];
      if (!localeData) {
        localeData = _helperFnc.getLocaleMap()["en-US"];
      }
      return localeData[key];
    },

    /**
     * sanitizeLocale sanitize a locale
     * @param locale = the locale to sanitize
     **/
    sanitizeLocale: function (locale) {
      if (locale) {
        var tempLocaleArray = locale.split("-");
        locale = tempLocaleArray[0] + (tempLocaleArray[1] ? "-" + tempLocaleArray[1].toUpperCase() : "");
      } else {
        locale = "en-US";
      }
      return locale;
    },

    /** returning a obj w/ url maps */
    getLocaleMap: function () {
      return {
        "fr-FR": {
          "today": "/fr-FR/temps/aujour/l/",
          "alert_details": "/fr-FR/temps/alertes/localalerts/l/",
          "g8Title": "Prévisions Météo significatives"
        },
        "ca-ES": {
          "today": "/ca-ES/temps/avui/l/",
          "alert_details": "/ca-ES/temps/alertes/localalerts/l/",
          "g8Title": "Previsió del temps significativa"
        },
        "cs-CZ": {
          "today": "/cs-CZ/pocasi/dnes/l/",
          "alert_details": "/cs-CZ/pocasi/Vystrahy/localalerts/l/",
          "g8Title": "Důležitá předpověď počasí"
        },
        "da-DK": {
          "today": "/da-DK/vejret/idag/l/",
          "alert_details": "/da-DK/vejret/varsler/localalerts/l/",
          "g8Title": "Vigtig vejrudsigt"
        },
        "el-GR": {
          "today": "/el-GR/weather/today/l/",
          "alert_details": "/el-GR/weather/alerts/localalerts/l/",
          "g8Title": "Σημαντική πρόγνωση καιρού"
        },
        "fi-FI": {
          "today": "/fi-FI/weather/today/l/",
          "alert_details": "/fi-FI/weather/alerts/localalerts/l/",
          "g8Title": "Σημαντική πρόγνωση καιρού"
        },
        "hr-HR": {
          "today": "/hr-HR/vrijeme/danas/l/",
          "alert_details": "/hr-HR/vrijeme/upozorenja/localalerts/l/",
          "g8Title": "Značajna vremenska prognoza"
        },
        "hu-HU": {
          "today": "/hu-HU/idojaras/ma/l/",
          "alert_details": "/hu-HU/idojaras/Riasztasok/localalerts/l/",
          "g8Title": "Fontos időjárás-előrejelzés"
        },
        "nl-NL": {
          "today": "/nl-NL/weer/vandaag/l/",
          "alert_details": "/nl-NL/weer/meldingen/localalerts/l/",
          "g8Title": "Significante weersverwachting"
        },
        "no-NO": {
          "today": "/no-NO/weather/today/l/",
          "alert_details": "/no-NO/weather/alerts/localalerts/l/",
          "g8Title": "Viktig værmelding"
        },
        "pl-PL": {
          "today": "/pl-PL/pogoda/dzisiaj/l/",
          "alert_details": "/pl-PL/pogoda/alerty/localalerts/l/",
          "g8Title": "Prognoza umiark. war. pogodowych"
        },
        "pt-BR": {
          "today": "/pt-BR/clima/hoje/l/",
          "alert_details": "/pt-BR/clima/alertas/localalerts/l/",
          "g8Title": "Previsão do tempo significativa"
        },
        "ro-RO": {
          "today": "/ro-RO/vreme/astazi/l/",
          "alert_details": "/ro-RO/vreme/avertizari/localalerts/l/",
          "g8Title": "Prognoză fenomene meteo semnificative"
        },
        "sk-SK": {
          "today": "/sk-SK/pocasie/dnes/l/",
          "alert_details": "/sk-SK/pocasie/vystrahy/localalerts/l/",
          "g8Title": "Podstatná predpoveď počasia"
        },
        "sv-SE": {
          "today": "/sv-SE/weather/today/l/",
          "alert_details": "/sv-SE/weather/alerts/localalerts/l/",
          "g8Title": "Betydande väderprognos"
        },
        "tr-TR": {
          "today": "/tr-TR/kisisel/bugun/l/",
          "alert_details": "/tr-TR/kisisel/uyarilari/localalerts/l/",
          "g8Title": "Önemli Hava Tahmini"
        },
        "uk-UA": {
          "today": "/uk-UA/weather/today/l/",
          "alert_details": "/uk-UA/weather/alerts/localalerts/l/",
          "g8Title": "Прогноз значимої погоди"
        },
        "zh-CN": {
          "today": "/zh-CN/weather/today/l/",
          "alert_details": "/zh-CN/weather/alerts/localalerts/l/",
          "g8Title": "重要天气预报"
        },
        "zh-HK": {
          "today": "/zh-CN/weather/today/l/",
          "alert_details": "/zh-CN/weather/alerts/localalerts/l/",
          "g8Title": "重要天气预报"
        },
        "zh-TW": {
          "today": "/zh-TW/weather/today/l/",
          "alert_details": "/zh-TW/weather/alerts/localalerts/l/",
          "g8Title": "重要天氣預報"
        },
        "hi-IN": {
          "today": "/hi-IN/weather/today/l/",
          "alert_details": "/hi-IN/weather/alerts/localalerts/l/",
          "g8Title": "महत्वपूर्ण मौसम पूर्वानुमान"
        },
        "id-ID": {
          "today": "/id-ID/cuaca/sekarang/l/",
          "alert_details": "/id-ID/cuaca/siaga/localalerts/l/",
          "g8Title": "Prakiraan Cuaca Signifikan"
        },
        "in-ID": {
          "today": "/id-ID/cuaca/sekarang/l/",
          "alert_details": "/id-ID/cuaca/siaga/localalerts/l/",
          "g8Title": "Prakiraan Cuaca Signifikan"
        },
        "ja-JP": {
          "today": "/ja-JP/weather/today/l/",
          "alert_details": "/ja-JP/weather/alerts/localalerts/l/",
          "g8Title": "悪天予測"
        },
        "ko-KR": {
          "today": "/ko-KR/weather/today/l/",
          "alert_details": "/ko-KR/weather/alerts/localalerts/l/",
          "g8Title": "중요 일기 예보"
        },
        "ms-MY": {
          "today": "/ms-MY/weather/today/l/",
          "alert_details": "/ms-MY/weather/alerts/localalerts/l/",
          "g8Title": "Ramalan Cuaca Signifikan"
        },
        "ru-RU": {
          "today": "/ru-RU/weather/today/l/",
          "alert_details": "/ru-RU/weather/alerts/localalerts/l/",
          "g8Title": "Важная информация о погоде"
        },
        "th-TH": {
          "today": "/th-TH/weather/today/l/",
          "alert_details": "/th-TH/weather/alerts/localalerts/l/",
          "g8Title": "การพยากรณ์อากาศที่สำคัญ"
        },
        "it-IT": {
          "today": "/it-IT/tempo/oggi/l/",
          "alert_details": "/it-IT/tempo/avvisi/localalerts/l/",
          "g8Title": "Previsioni meteo significative"
        },
        "vi-VN": {
          "today": "/vi-VN/weather/today/l/",
          "alert_details": "/vi-VN/weather/alerts/localalerts/l/",
          "g8Title": "Dự báo thời tiết đáng chú ý"
        },
        "pt-PT": {
          "today": "/pt-PT/clima/hoje/l/",
          "alert_details": "/pt-PT/clima/alertas/localalerts/l/",
          "g8Title": "Previsão meteorológica significativa"
        },
        "es-ES": {
          "today": "/es-ES/tiempo/hoy/l/",
          "alert_details": "/es-ES/tiempo/alertas/localalerts/l/",
          "g8Title": "Pronóstico de tiempo significativo"
        },
        "de-DE": {
          "today": "/de-DE/wetter/heute/l/",
          "alert_details": "/de-DE/unwetter/alarm_details/l/",
          "g8Title": "Wichtige Wettervorhersage"
        },
        "en-GB": {
          "today": "/en-GB/weather/today/l/",
          "alert_details": "/en-GB/weather/alerts/localalerts/l/",
          "g8Title": "Significant Weather Forecast"
        },
        "es-US": {
          "today": "/es-US/tiempo/hoy/l/",
          "alert_details": "/es-US/tiempo/alertas/localalerts/l/",
          "g8Title": "Pronóstico de tiempo significativo"
        },
        "en-US": {
          "today": "/weather/today/l/",
          "alert_details": "/weather/alerts/localalerts/l/",
          "g8Title": "Significant Weather Forecast"
        },
        "en-IN": {
          "today": "/en-IN/weather/today/l/",
          "alert_details": "/en-IN/weather/alerts/localalerts/l/",
          "g8Title": "Significant Weather Forecast"
        }
      };
    },

    /**
     * Initiating push notification events
     * Adding event listeners on self obj
     * */
    initAddingEvents: function () {
      // Extending self by adding addEventListener for all of Events props
      _helperFnc.extend(self, Events, 'addEventListener');
    }
  };


  /**
   * 3. Extending Events w/ eventListeners
   * Events listeners Including:
   * install, activate, push, notificationclick events
   */
  _helperFnc.extend(Events, {
    /** install event handler */
    install: function (event) {
      self.skipWaiting();
    },

    /** activate event handler */
    push: function (event) {
      event.waitUntil(
        PushEvent.delayPromise(700)// Wait 700ms, before doing anything..
          .then(PushEvent.getDeviceToken)// then get token
          .then(PushEvent.fetchDSXForDeviceToken)// then fetch data
          .then(PushEvent.getNotificationContentForDSX)// then generate notifications
          .then(PushEvent.showNotifications)// then show notifications
          .then(PushEvent.successfullySent)// then show notifications
        //.catch() //TODO: add logic for error handling
      );
    },

    /** notificationclick event handler */
    notificationclick: function (event) {
      // Closing notification
      event.notification.close();

      // Configuring the url
      var urlValue = event.notification && event.notification.data.url,
        url = (urlValue ? urlValue : baseUrl);

      // Extending event
      event.waitUntil(
        clients.matchAll({
          type: 'window'
        }).then(function (windowClients) {
          _helperFnc.forEach(windowClients, function (windowClient, index) {
            var client = windowClient;
            if (client.url === url && 'focus' in client) {
              return client.focus();
            }
          });

          if (clients.openWindow && url !== 'None') {
            return clients.openWindow(url);
          }
        })
      );
    }
  });


  /**
   * Extend PushEvent
   * When a push notification event occurs these functions will handle the push event
   * */
  _helperFnc.extend(PushEvent, {
    /**
     * 1. delayPromise
     * Delays and returns a promise for the backend.
     * @param delay = time to delay
     * **/
    delayPromise: _helperFnc.returnPromise(function (delay, resolve) {
      setTimeout(function () {
        return resolve();
      }, delay);
    }),

    /**
     * 2. getDeviceToken
     * Get the user token then return a promise, getSubscription return's a promise
     * **/
    getDeviceToken: function () {
      return self.registration.pushManager.getSubscription();
    },

    /**
     * 3. fetchDSXForDeviceToken
     * Gets the token then fetches the data, returns a promise
     * @param data = user token, from getDeviceToken function
     * **/
    fetchDSXForDeviceToken: _helperFnc.returnPromise(function (data, resolve, reject) {
      self.deviceToken = data && data.endpoint && data.endpoint.substring(data.endpoint.lastIndexOf('/') + 1);
      fetch(DSX_NOTIFICATION_QUEUE + self.deviceToken, {
      //fetch(DSX_NOTIFICATION_QUEUE_TEST, {
        method: 'POST',
        mode: 'cors'
      }).then(function (response) {
        return response.json();
      }).then(function (dsxData) {
        dsxData && resolve(dsxData);
      }).catch(function(){
        reject();
      });
    }),

    /**
     * 4. getNotificationContentForDSX
     * call the pushResponse factory functions then return a promise w/ notification list
     * @param data = the fetched DSX data
     * **/
    getNotificationContentForDSX: _helperFnc.returnPromise(function (data, resolve) {
      if (Array.isArray(data)) {
        var notificationData, notificationList = [];
        for (var i = 0; i < data.length; i++) {
          notificationData = _helperFnc.pushResponse(data[i], i);
          if (notificationData) {
            notificationList.push(notificationData);
          }
        }
        resolve(notificationList);
      }
    }),

    /**
     * 5. Showing notification alert helper fnc
     * Iterating through notificationArray and sending notifications, returns a promise to extend the push event
     * @param notificationArray = the notification array with notification objects
     * */
    showNotifications: _helperFnc.returnPromise(function (notificationArray, resolve) {
      var length = notificationArray.length, count = 0, i;
      // If notificationArray is empty, then resolving
      (length === 0) && resolve();

      // Iterating through notificationArray and sending notifications, then resolving
      for (i = 0; i < length; i++) {
        (function (key, index) {
          return self.registration.showNotification(key.title || pushFallBack.title, {
            body: key.body || pushFallBack.body,
            icon: key.icon || pushFallBack.icon,
            tag: key.tag || pushFallBack.tag,
            data: key.urlData || pushFallBack.data,
            requireInteraction: false,
            bs: {key: key, index: index}
          }).then(function () {
            count++;
            count === length && resolve(true);
          });
        })(notificationArray[i], i);
      }
    }),

    /**
     * 6. successfullySent
     * function is called whenever a notification has successful sent
     * @param successfullySent = boolean
     * */
    successfullySent: function (successfullySent) {
      //console.dir(successfullySent);
    }
    // todo extend PushEvent w/ error handler here:
  });


  /** 5. Initiating adding events on self */
  _helperFnc.initAddingEvents();

})();
�A�Eo��   JyeR         oW �L�kū. 9q�kū. �  HTTP/1.1 200 status:200 accept-ranges:bytes content-encoding:gzip content-type:application/javascript etag:"5265-53d23ccb6172b-gzip" last-modified:Fri, 23 Sep 2016 02:47:31 GMT x-cache-hits:19 x-varnish:368378468 368281259 x-varnish-cache:HIT content-length:5704 cache-control:public, max-age=900 date:Sat, 15 Apr 2017 07:02:10 GMT vary:Accept-Encoding access-control-max-age:86400 access-control-allow-credentials:false access-control-allow-headers:* access-control-allow-methods:GET,POST access-control-allow-origin:* property-id:drupal-prod x-theme-asset:1 x-origin-hint:dna twc-geoip-latlong:32.8548,-117.2497 twc-geoip-country:US twc-device-class:desktop twc-locale-group:US twc-connection-speed:4G        ]  0�Y0� �2d�}]��\�b/dF0
*�H�=0��10	UUS10U
Symantec Corporation10USymantec Trust Network110/U(Symantec Class 3 ECC 256 bit SSL CA - G20170413000000Z180413235959Z0��10	UUS10UGeorgia10UAtlanta1(0&U
TWC Product and Technology, LLC10UDigital10Uwww.weather.com0Y0*�H�=*�H�=B IP}��˖P	�Te!���M��D�a鵣�L��D$>�!���ٰc2ݏ�6�-�HwUB��*��Q0�M0��U��0��
*.wxug.com�wunderground.com�wxug.com�weather.com�imwx.com�*.weather.com�wsi.com�w-x.co�
*.imwx.com�www.business.weather.com�crazimals.com�*.w-x.co�www.crazimals.com�*.wunderground.com�	*.wsi.com�stg.crazimals.com�www.weather.com0	U0 0U��0aU Z0X0Vg�0L0#+https://d.symcb.com/cps0%+0https://d.symcb.com/rpa0+U$0"0 ���http://rc.symcb.com/rc.crl0U%0++0U#0�%���Kz��
��S�x���0W+K0I0+0�http://rc.symcd.com0&+0�http://rc.symcb.com/rc.crt0�
+�y���� � v ��+zO� ����hp~.���\��=�Ͷ��  [iBM5   G0E! �}r�K6�R~����L}+��;�)�M��g�<7� �ˢ���%&����۹W�
��N}E�ٶrC� u ��	��X����gp
<5��߸�w���  [iBMl   F0D '���~��@f�c���%2bC�� ���@���� 3U��������n%������t��ȺEKq&j�0
*�H�=G 0D L�D�d'pQx��f�u똌+���ϴ���Ǫ A �n$�K�"��F����4��Q�   n  0�j0�R�?������z��(.wZ�0	*�H�� 0��10	UUS10U
VeriSign, Inc.10UVeriSign Trust Network1:08U1(c) 2006 VeriSign, Inc. - For authorized use only1E0CU<VeriSign Class 3 Public Primary Certification Authority - G50150512000000Z250511235959Z0��10	UUS10U
Symantec Corporation10USymantec Trust Network110/U(Symantec Class 3 ECC 256 bit SSL CA - G20Y0*�H�=*�H�=B ������NN�d[�nӫ8�D�@\m���7�y��g����c;F���S�ܗ>+�F��9��]0�Y0.+"0 0+0�http://s.symcd.com0U�0� 0eU ^0\0Z
`�H��E60L0#+https://d.symcb.com/cps0%+0https://d.symcb.com/rpa0/U(0&0$�"� �http://s.symcb.com/pca3-g5.crl0U�0+U$0"� 010USYMC-ECC-CA-p256-220U%���Kz��
��S�x���0U#0��e�����0	�C9��3130	*�H�� � 0e��u��1�p�j��B��_M�̠zFw��u���3����Pz�A�Dk�?�Ñ�R�Vȅ�����$ٷ�^��a��R@�e$ˈ+����u��/�?���D}{/q��%�b�����m��p9�g����߳�2f�02w��N�0�Z�1g˲ҋ0tV�i}�� O��֥.�����B�~��F �(�1�0���|WhЏ_t��Y��GC�9��Lts}�`/�M������!��v�O��k���  �  0��0����ў&}�J!X��k;J0	*�H�� 0��10	UUS10U
VeriSign, Inc.10UVeriSign Trust Network1:08U1(c) 2006 VeriSign, Inc. - For authorized use only1E0CU<VeriSign Class 3 Public Primary Certification Authority - G50061108000000Z360716235959Z0��10	UUS10U
VeriSign, Inc.10UVeriSign Trust Network1:08U1(c) 2006 VeriSign, Inc. - For authorized use only1E0CU<VeriSign Class 3 Public Primary Certification Authority - G50�"0	*�H�� � 0�
� �$)z5�`��K;N�|�<E�+��)�W�d�'���1�]"�*��B����U���K���~W��C�fba�`������b=T�I�YT�&�+�������3I�CcjRKҏ�pQMщi{�p���t�{]KVӖ�w����%��g��� ��:����<����7���׬���=��u�3@�t$!����*RǏ�I�cG�<i���G�+~Oŕ����C�gs�~�?�s�3
�]?4����S�% ���0��0U�0�0U�0m+a0_�]�[0Y0W0U	image/gif0!00+�������k�πj�H,{.0%#http://logo.verisign.com/vslogo.gif0U�e�����0	�C9��3130	*�H�� � �$J0_b���/=�ܙ-�w��y"8�ħ�x�bEpd��f-�	~_���(e��G���|�Zi �>mn<n���@������A��� ��d8�ɔ)o���%ۤ�D�AziJXO`�~�j�%9���e*�����^��-�����*m(���&��o����B2�ΝQ^(����[�}E@r��k�35Hq��'��e_�v�Dz��\�]23�T�?h\�BJ�8T�_��,�֨�cj        ,�P            ��+zO� ����hp~.���\��=�Ͷ��׃s��.           G   0E! �}r�K6�R~����L}+��;�)�M��g�<7� �ˢ���%&����۹W�
��N}E�ٶrC�        Symantec log           ��	��X����gp
<5��߸�w���ୄs��.           F   0D '���~��@f�c���%2bC�� ���@���� 3U��������n%������t��ȺEKq&j�         Google 'Pilot' log     �zv.3>���3��U��D   23.63.48.252�     h2        �q��&�7����'��N�?�>f3��K�ڞ?�A�Eo��   �AU�$      