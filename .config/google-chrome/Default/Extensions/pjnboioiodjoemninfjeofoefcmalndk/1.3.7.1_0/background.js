chrome.runtime.onInstalled.addListener(function(details) {
     // open splash page
     if(details.reason == "install"){
         console.log("First install");
         chrome.tabs.create({url: "splash.html"});
     }else if(details.reason == "update"){
         var thisVersion = chrome.runtime.getManifest().version;
         console.log("Updated from " + details.previousVersion + " to " + thisVersion + "!");
     }
});

chrome.extension.onRequest.addListener(function(request, sender, sendResponse) {

    // GET request
    if (request.storage) localStorage[request.storage] = request.value;

    // respond to requests to access localStorage
    if (request.method == "username")
      sendResponse({status: localStorage[window.btoa('pid')]});
    else if (request.method == "password")
      sendResponse({status: localStorage[window.btoa('pwd')]});
    else if (request.method == "schedule")
      sendResponse({status: localStorage['schedule']});
    else
      sendResponse({});

});