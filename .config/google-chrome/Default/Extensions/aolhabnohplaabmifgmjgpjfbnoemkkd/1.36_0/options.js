// Saves options to chrome.storage.sync.
function save_options() {
  chrome.storage.sync.set({
    deactivated: document.getElementById('deactivate').checked
  }, function() {
    location.reload();
    chrome.tabs.getAllInWindow(null, function(tabs) {
        for(var i = 0; i < tabs.length; i++) {
          if (tabs[i].url.indexOf("google.") > -1)
            chrome.tabs.update(tabs[i].id, {url: tabs[i].url});
        }
    }); 
    window.close();
  });
}

function restore_options()
{
  chrome.storage.sync.get({
    deactivated: false
  }, function(items) {
    document.getElementById('deactivate').checked = items.deactivated;
  });
}
document.addEventListener('DOMContentLoaded', restore_options);
document.getElementById('save').addEventListener('click', save_options);