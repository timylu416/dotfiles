// Script run on MyTritonLink login page
// Inserts saved PID and password into form fields

(function (chrome) {

    // find fiends in login form
    var loginField = document.getElementById('urn:mace:ucsd.edu:sso:studentsso:username');
    var passField = document.getElementById('urn:mace:ucsd.edu:sso:studentsso:password');

    // If fields are null, means new version of SSO login page - change vars accordingly
    if(loginField == null) {
        loginField = document.getElementById('ssousername');
    }
    if(passField == null) {
        passField = document.getElementById('ssopassword');
    }

    // request user/pass data from localStorage
    // fill in username and password
    chrome.extension.sendRequest({method: "username"}, function(response) {
      var username = window.atob(response.status);
      loginField.value = username;
    });
    chrome.extension.sendRequest({method: "password"}, function(response) {
      var password = window.atob(response.status);
      passField.value = password;
    });

}(chrome));