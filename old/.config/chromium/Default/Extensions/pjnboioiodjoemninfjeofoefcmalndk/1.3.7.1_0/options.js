//Saves options to localStorage.
function save_options() {
  var pid = document.getElementById("txt_pid");
  var pwd = document.getElementById("txt_pwd");
  localStorage[window.btoa("pid")] = window.btoa(pid.value);
  localStorage[window.btoa("pwd")] = window.btoa(pwd.value);

  // Update status to let user know options were saved.
  var status = document.getElementById("save");
  status.innerHTML = "Saving";
  setTimeout(function() {
    status.innerHTML = "Saved";
  }, 750);

  var currentpid = document.getElementById("current-pid");
  currentpid.innerHTML = window.atob(localStorage[window.btoa("pid")]);
}
document.querySelector('#save').addEventListener('click', save_options);

function init() {
    var currentpid = value = document.getElementById("current-pid");
    currentpid.innerHTML = window.atob(localStorage[window.btoa("pid")]);
}

document.addEventListener("DOMContentLoaded", init, false);