/**
 * Created by cpacker on 3/2/14.
 */

// Saves options to localStorage.
function clear_options() {

    // Update status to let user know options were saved.
    var status = document.getElementById("clear");
    status.innerHTML = "Clearing";

    localStorage[window.btoa("pid")] = "";
    localStorage[window.btoa("pwd")] = "";

    setTimeout(function() {
        status.innerHTML = "Cleared";
    }, 750);

    var currentpid = document.getElementById("current-pid");
    currentpid.innerHTML = window.atob(localStorage[window.btoa("pid")]);

    if(currentpid.innerHTML == "") currentpid.innerHTML = "None";
}
document.querySelector('#clear').addEventListener('click', clear_options);

// Opens options.html in a new page
function open_options() {
    window.open(chrome.extension.getURL("options.html"));
}
document.querySelector('#options').addEventListener('click', open_options);

// Update HTML inside schedule section
var schedulecode = localStorage["schedule"];
if (typeof schedulecode != 'undefined') {
    $("#class_schedule").html(schedulecode);
}

// Close other sections when the schedule section is opened
document.addEventListener("DOMContentLoaded", init, false);

var oneD;
var twoD;
var threeD;

function init() {

    var currentpid = value = document.getElementById("current-pid");
    currentpid.innerHTML = window.atob(localStorage[window.btoa("pid")]);
    if(currentpid.innerHTML == "") currentpid.innerHTML = "None";

    oneD = document.querySelector("#done");
    twoD = document.querySelector("#dtwo");
    threeD = document.querySelector("#dthree");
    var summaryone = document.querySelector("#done summary");
    var summarytwo = document.querySelector("#dtwo summary");
    var summarythree = document.querySelector("#dthree summary");

    summaryone.addEventListener("click", function(e) {
        var open = !oneD.hasAttribute("open");
        if(open) {
            twoD.removeAttribute("open");
            threeD.removeAttribute("open");
        }
    }, false);
    summarytwo.addEventListener("click", function(e) {
        var open = !twoD.hasAttribute("open");
        if(open) {
            oneD.removeAttribute("open");
            threeD.removeAttribute("open");
        }
    }, false);
    summarythree.addEventListener("click", function(e) {
        var open = !threeD.hasAttribute("open");
        if(open) {
            oneD.removeAttribute("open");
            twoD.removeAttribute("open");
        }
    }, false);

}

// Fixes links in schedule HTML
$(document).ready(function(){
    $('#class_schedule').on('click', 'a', function(){
        var refurl;
        if( ($(this).attr('href')).indexOf("/cgi-bin/tritonlink.pl/4/students/academic/display_grid.pl?") !== -1) {
            refurl = 'https://act.ucsd.edu/' + $(this).attr('href');
        }else if ( ($(this).attr('href')).indexOf("studentEnrolledClasses/enrolledclasses") !== -1) {
            refurl = 'https://act.ucsd.edu/' + $(this).attr('href');
        }else if ( ($(this).attr('href')).indexOf("myTritonlink20/") !== -1) {
            refurl = 'https://act.ucsd.edu/' + $(this).attr('href');
        }else {
            refurl = $(this).attr('href');
        }
        chrome.tabs.create({url: refurl});
        return false;
    });
});