/**
 * Author: Tim Lu
 * PID: A13225107
 * Email: tylu@ucsd.edu
 * Date: `TODO`
 * Filename: ActorGraph.cpp
 * Description:
 *  This file is meant to exist as a container for starter code that you can use
 *  to read the input file format defined in movie_casts.tsv. Feel free to
 *  modify any/all aspects as you wish.
 */


#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include "ActorGraph.h"

using namespace std;

ActorGraph::ActorGraph(void) {}

bool ActorGraph::loadFromFile(const char* in_filename, bool use_weighted_edges)
{
    // Initialize the file stream
    ifstream infile(in_filename);
    if(infile.fail()){
        cout << "Failed to read database file!!!" << endl;
        return -1;
    }

    bool have_header = false;

    // keep reading lines until the end of file is reached
    while (infile) {
        string s;

        // get the next line
        if (!getline( infile, s )) break;

        if (!have_header) {
            // skip the header
            have_header = true;
            continue;
        }

        istringstream ss( s );
        vector <string> record;

        while (ss) {
            string next;

            // get the next string before hitting a tab character and put it in
            // 'next'
            if (!getline( ss, next, '\t' )) break;

            record.push_back( next );
        }

        if (record.size() != 3) {
            // we should have exactly 3 columns
            continue;
        }

        string actor_name(record[0]);
        string movie_title(record[1]);
        int movie_year = stoi(record[2]);
        string hashIndex = movie_title + to_string(movie_year);
        // we have an actor/movie relationship, now what?

        // create new Actor/Movie Node if necessary (run find first)
        ActorNode* actorPtr = findActor(actor_name);
        MovieNode* moviePtr = findMovie(movie_title);
        if(actorPtr == nullptr){
            actorPtr = addActor(actor_name);
        }
        if(moviePtr == nullptr){
            moviePtr = addMovie(movie_title, movie_year);
        }

        //create 2 way connection between actor and movie
        actorPtr->addMovie(moviePtr);
        moviePtr->addActor(actorPtr);

    }

    if (!infile.eof()) {
        cerr << "Failed to read " << in_filename << "!\n";
        return false;
    }
    infile.close();

    return true;
}


/** Finds if an Actor already exists as a node in the graph and returns
 *  pointer to that actor's node if found.
 *  Parameters:
 *      name - name of actor to find.
 *  Return:
 *      Pointer to node repsenting that actor or null pointer if actor
 *      not found.
 */
ActorNode* ActorGraph::findActor(string name){
    //search for actor using name as key
    std::unordered_map<string, ActorNode>::iterator search = actors.find(name);
    //actor not found return null
    if(search == actors.end()){
        return nullptr;
    }
    //actor found, return pointer to actor's node
    return &(search->second);

}


/** Adds an actor to the graph if not already there.
 *  Parameters:
 *      name - name of actor to add.
 */
ActorNode* ActorGraph::addActor(string name) {
    //exit if actor already exists
    //std::unordered_map<string, ActorNode>::iterator search = actors.find(name);
    //if(search != actors.end()){
    //    return nullptr;
    //}

    //continue
    ActorNode newActor(name);
    //get pair returned by insert function
    auto returnedPair = actors.insert(make_pair(name, newActor));
    //check that insertion successful via boolean
    if(!returnedPair.second){
        return nullptr;
    }
    //return pointer to node which is inside nested pair
    return &((returnedPair.first)->second);
}


/** Checks if Actor exists as a node in the graph and returns pointer to
 * the specific movie's node if found, else returns a nullptr.
 * Parameters:
 *      name - name of movie to find
 * Return:
 *      Pointer to node representing movie, nullptr if no such node exists.
 */
MovieNode* ActorGraph::findMovie(string name, int year){
    string hashIndex = name + to_string(year);
    //search for movie using name as key
    std::unordered_map<string, MovieNode>::iterator search = movies.find(hashIndex);
    //actor not found return null
    if(search == movies.end()){
        return nullptr;
    }
    //actor found, return pointer to actor's node
    return &(search->second);
}


/** Adds a movie to the graph if not already there.
 * Parameters:
 *      name - name of movie to add.
 * Return:
 *      Pointer to newly created movie node
 */
MovieNode* ActorGraph::addMovie(string name, int year){
    //exit if movie already exists
    // std::unordered_map<string, MovieNode>::iterator search = movies.find(name);
    //if(search != movies.end()){
    //  return nullptr;
    //}
    string hashIndex = name + to_string(year);

    //continue
    MovieNode newMovie(name, year);
    //get pair returned by insert function
    auto returnedPair = movies.insert(make_pair(hashIndex, newMovie));
    //check that insertion successful via boolean
    if(!returnedPair.second){
        return nullptr;
    }
    //return pointer to node which is inside nested pair
    return &((returnedPair.first)->second);

}


//TODO DEBUG PRINT SIZE
void ActorGraph::printSize(){
    cout << "Actor Nodes: " << actors.size() << endl
        << "Movie Nodes: " << movies.size() << endl;
}


