0\r�m��   T   �|DF    https://s3-us-west-2.amazonaws.com/zytools-standalone/vendor/zyWebResourceManager.jswindow.require.define({"zyWebResourceManager": function(exports, require, module) {
function zyWebResourceManager() {
    // These differ between zyTool's and zyWeb's implementation.
    this.baseUrl     = 'https://zytools.zybooks.com/54/';
    this.resourceUrl = 'https://zytools.zybooks.com/54/vendor/';
    this.getDependencyURL = function(dependencyPath) {
        return (this.resourceUrl + dependencyPath);
    }
    // Legacy support: |getImageURL| should not be called by zyTools.
    this.getImageURL = function(imagePath, toolName) {
        console.error('getImageURL should not be called by zyTools. Instead, use getResourceURL. Also, instead of an img folder, use a folder named resource.');
    }
    this.getResourceURL = function(resourcePath, toolName) {
        return ('https://zytools.zybooks.com/54/' + toolName + '/resource/' + resourcePath);
    }
    this.getPathToToolJS = function(resourceName) {
        return (this.baseUrl + resourceName + '/' + resourceName + '.js');
    }
    this.getZydeHost = function() {
        return '';
    }
    this.getImageContentResourceUrl = function(subject, filename) {
        return '';
    }

    // Legacy support: |functionUsed| should not be called by zyTools.
    this.legacySupportErrorForDependencies = function(functionUsed) {
        console.error(functionUsed + ' should not be called by zyTools. Instead, use a dependencies.js file to declare dependencies.');
    }
    this.getResource = function(tool) {
        this.legacySupportErrorForDependencies('getResource');
    }

    // "Sets" to prevent reloading of resources.
    this.toolsCache     = {};
    this.vendorJSCache  = {};
    this.vendorCSSCache = {};

    /*
        Add |dependencies| that haven't loaded yet to |toolsToLoad|, |vendorJSToLoad|, and |vendorCSSToLoad|.
        |dependencies| is an object containing:
            * |tools| is an array of tool names.
            * |vendorJS| is an array of vendor Javascript file paths.
            * |vendorCSS| is an array of vendor CSS files.
        |toolsToLoad| is an array of tool names.
        |vendorJSToLoad| is an array of vendor Javascript file paths.
        |vendorCSSToLoad| is an array of vendor CSS files.
    */
    this.addDependenciesToLoad = function(dependencies, toolsToLoad, vendorJSToLoad, vendorCSSToLoad) {
        // If dependencies exist, then add them to be loaded.
        if (dependencies) {
            var tools     = dependencies.tools;
            var vendorJS  = dependencies.vendorJS;
            var vendorCSS = dependencies.vendorCSS;

            var self = this;
            if (tools) {
                tools.forEach(function(tool) {
                    // If the tool has not been loaded, then add tool to respective set.
                    if (!(tool in self.toolsCache)) {
                        toolsToLoad.push(tool);
                    }
                });
            }

            if (vendorJS) {
                vendorJS.forEach(function(js) {
                    // If the js has not been loaded, then add js to respective set.
                    if (!(js in self.vendorJSCache)) {
                        vendorJSToLoad.push(js);
                    }
                });
            }

            if (vendorCSS) {
                vendorCSS.forEach(function(css) {
                    // If the tool has not been loaded, then add tool to respective set.
                    if (!(css in self.vendorCSSCache)) {
                        vendorCSSToLoad.push(css);
                    }
                });
            }
        }
    }

    /*
        Load vendor scripts, vendor style sheets, and tool resources stored in |dependencies|.
        |dependencies| is a required object containing:
            * |tools| is an array of tool names. Ex: ['progressionTool', 'utilities'].
            * |vendorJS| is an array of vendor Javascript file paths. Ex: ['jquery.mousewheel.js', 'jquery-ui/jquery-ui.min.js'].
            * |vendorCSS| is an array of vendor CSS files. Ex: ['normalize.css', 'jquery-ui/jquery-ui.css'].
        |vendorJS|, |vendorCSS|, and |tools| are optional.

        The dependency types load in sequence, from |tools| -> |vendorCSS| -> |vendorJS|.
        A queue for each dependency type is maintained.
        The tools queue is loaded first in order to collect all needed |tools|, |vendorCSS|, and |vendorJS| dependencies.
        The vendor CSS queue is loaded second b/c there is no way to get a callback after a CSS file has loaded.
        So, a zyTool may be rendered before a dependency vendor CSS file has downloaded, causing the zyTool to be in an unstyled initial state.
    */
    this.getDependencies = function(dependencies) {
        // Legacy support: getDependencies should not be called by zyTools.
        if (arguments.length > 1) {
            this.legacySupportErrorForDependencies('getDependencies');
        }

        var toolsToLoad     = [];
        var vendorJSToLoad  = [];
        var vendorCSSToLoad = [];

        this.addDependenciesToLoad(dependencies, toolsToLoad, vendorJSToLoad, vendorCSSToLoad);

        var self = this;
        return new Ember.RSVP.Promise(function(resolve, reject) {
            function loadTool() {
                // If no tools to load, then move on to loading |vendorJSToLoad|.
                if (toolsToLoad.length === 0) {
                    loadVendorCSS();
                }
                // Otherwise, load the next tool.
                else {
                    var tool = toolsToLoad[0];
                    zyRequireJS([self.getPathToToolJS(tool)],
                        function() {
                            var moreDependenciesToLoad = require(tool).dependencies;
                            self.addDependenciesToLoad(moreDependenciesToLoad, toolsToLoad, vendorJSToLoad, vendorCSSToLoad);

                            // Remove first element from |toolsToLoad|.
                            toolsToLoad.shift();

                            // Add loaded tool to cache.
                            self.toolsCache[tool] = true;

                            loadTool();
                        },
                        function(error) {
                            reject(error);
                        }
                    );
                }
            }

            function loadVendorCSS() {
                if (vendorCSSToLoad) {
                    vendorCSSToLoad.forEach(function(css) {
                        // Add the stylesheet to the page.
                        $('head').append('<link rel="stylesheet" type="text/css" href="' + self.getDependencyURL(css) + '">');

                        // Add loaded css to cache.
                        self.vendorCSSCache[css] = true;
                    });
                }

                loadVendorJS();
            }

            function loadVendorJS() {
                // If no vendorJS to load, then move on to loading |vendorCSSToLoad|.
                if (vendorJSToLoad.length === 0) {
                    resolve();
                }
                else {
                    var js       = vendorJSToLoad[0];
                    var fullPath = self.resourceUrl + js;
                    zyRequireJS([fullPath],
                        function() {
                            // Remove first element from |vendorJSToLoad|.
                            vendorJSToLoad.shift();

                            // Add loaded js to cache.
                            self.vendorJSCache[js] = true;

                            loadVendorJS();
                        },
                        function(error) {
                            reject(error);
                        }
                    );
                }
            }

            loadTool();
        });
    }
}

var manager = new zyWebResourceManager();

module.exports = manager;
}});

�A�Eo��   ����      �  E �����. �ɰ���. i  HTTP/1.1 200 OK x-amz-id-2: HGeSg6wlF1Go2mlnVGhODphMa3lAdqcqbLL1FZPJe1bGt4mR2nyCrG33BUROJcs+wgh8bhc3EiI= x-amz-request-id: D772BCA987286AAF Date: Tue, 18 Apr 2017 04:29:24 GMT Last-Modified: Mon, 19 Dec 2016 19:55:04 GMT ETag: "dd565c823a1b42c0c68f68216e372729" Accept-Ranges: bytes Content-Type: application/x-javascript Content-Length: 7886 Server: AmazonS3          0�0���	d�O��.P���0	*�H�� 0d10	UUS10U
DigiCert Inc10Uwww.digicert.com1#0!UDigiCert Baltimore CA-2 G20160718000000Z171026120000Z0u10	UUS10U
Washington10USeattle10U
Amazon.com Inc.1%0#U*.s3-us-west-2.amazonaws.com0�"0	*�H�� � 0�
� ��Z#}��ֈJ���G�	�Q]�57�5#�a܊3���)9�!>1h��s��Vc�|A4Q{2��ܮ�s���0�]�Z1�]��XM,�np��A���9c����NJZ�qg��P^H)�*�r*7�/ldy\���@̃��+�WN�%L�h��QWwe�$J�J�1��T��ؕLF��Z4,O���8I�G!��H�U�J�G,ΐ@�s_a�k��k�?�~勾�	��SJAn�1�_�;,x� ���0��0U#0���(thFg�p%t E[}\D0U]Ihcna_�X`\����8�=�0��U��0�ւs3-us-west-2.amazonaws.com�*.s3-us-west-2.amazonaws.com�s3.us-west-2.amazonaws.com�*.s3.us-west-2.amazonaws.com�$s3.dualstack.us-west-2.amazonaws.com�&*.s3.dualstack.us-west-2.amazonaws.com�*.s3.amazonaws.com0U��0U%0++0��Uz0x0:�8�6�4http://crl3.digicert.com/DigiCertBaltimoreCA-2G2.crl0:�8�6�4http://crl4.digicert.com/DigiCertBaltimoreCA-2G2.crl0LU E0C07	`�H��l0*0(+https://www.digicert.com/CPS0g�0y+m0k0$+0�http://ocsp.digicert.com0C+0�7http://cacerts.digicert.com/DigiCertBaltimoreCA-2G2.crt0U�0 0	*�H�� � `}����vr����,(ɝ�v�y9�����ev�iXBd��4�DY��ۯ�Kݪ�ma}�qJ1��1��7:A��[���e �V�Ȱ�Z9p�'���W��|�1O�� �v;y�_��˗! ^���Q���@��r�[ey7,��E߱��nӷ�B��J���:R��&MP�$��
D���]Gp�O9�z>�W��-��N1gD�u�[װ�V�Lo���i��u�;�R�]���mj�/ȭ�A/��#+~&��ͼ� g  0�c0�K���	���&�;'���0	*�H�� 0Z10	UIE10U
	Baltimore10U
CyberTrust1"0 UBaltimore CyberTrust Root0151208120507Z250510120000Z0d10	UUS10U
DigiCert Inc10Uwww.digicert.com1#0!UDigiCert Baltimore CA-2 G20�"0	*�H�� � 0�
� �� � s�\u�\ps��z�#���?�!�M��-��1��k�]�Qհ^r��fԄ�A�	&��i�S�O>��:C+�Y��Ѻ�Y@rZg���U�	+����̩Կ���%�E��음�t���eP���E��/��6��ͲR��	�6q+��Л���[[9� ��Ӿ�������Z��/r��g����J� ���r�	�OB�M'BM���u�6`�&T���7����)Y ϻ%�՞������
� ��0�0U��(thFg�p%t E[}\D0U#0��Y0�GX̬�T6�{:�M�0U�0� 0U��04+(0&0$+0�http://ocsp.digicert.com0:U3010/�-�+�)http://crl3.digicert.com/Omniroot2025.crl0=U 60402U  0*0(+https://www.digicert.com/CPS0	*�H�� � /�7f�ϑU�)�P����(�ti;D0=��I�h6��0���IBcFR�iƛI�W�����u���3�b�CT�c��S��䦦��ex�3���>�p����x�3���,X��@�mA��ٳ�/�pk�:�{��K��o)���T�T��� �i,�/?P�.�W��sҡ$���E��)�f��l�O�+�LG���A��FwB����%K�PW��N?�%�A��bmo������)������T�KIǘ {  0�w0�_�  �0	*�H�� 0Z10	UIE10U
	Baltimore10U
CyberTrust1"0 UBaltimore CyberTrust Root0000512184600Z250512235900Z0Z10	UIE10U
	Baltimore10U
CyberTrust1"0 UBaltimore CyberTrust Root0�"0	*�H�� � 0�
� ��"��=W�&r��y�)��蕀���[�+)�dߡ]��	m�(.�b�b�����8�!��A+R{�w��Ǻ���j	�s�@����b�-��PҨP�(���%�����g�?���R/��pp����˚���3zw����hDBH��¤�^`������Y�Y�c��c��}]�z�����^�>_��i��96ru�wRM�ɐ,�=��#S?$�!\�)��:��n�:k�tc3�h1�x�v����]*��M��'9 �E0C0U�Y0�GX̬�T6�{:�M�0U�0�0U�0	*�H�� � �]��oQhB�ݻO'%���d�-�0���))�y?v�#�
�X��ap�aj��
�ż0|��%��@O�̣~8�7O��h1�Lҳt�u^Hp�\��y����e����R79թ1z��*����E��<^����Ȟ|.Ȥ�NKm�pmkc�d�����.���P�s������2�����~5���>0�z�3=�e����b�GD,]��2�G�8.����2j��<����$B�c9�     �   /�P    52.218.160.68   �        �'�������hb#���H���K��%�R��A�Eo��   s$�      