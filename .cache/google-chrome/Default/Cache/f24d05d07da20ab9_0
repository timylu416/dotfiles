0\r�m��   >   �Q�    http://doyle.wcdsb.ca/ICE4MI/digitial_electronics/minterms.htm<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Minterms</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../doyle.css" rel="stylesheet" type="text/css">
</head>

<body>
<h1>Designing Logic Circuits - Minterms</h1>
<p>The design process of a logic circuit can be broken down in the following steps:</p>
<ol>
  <li>Start with the purpose or function of the circuit.</li>
  <li>Choose logic variables that can be used to describe the function of the 
    circuit. </li>
  <li>Write the function as a truth table, showing how the circuit should respond 
    to all possible combinations of inputs. </li>
  <li>Write a boolean expression that represents the truth table.</li>
  <li>Simplify the boolean expression. </li>
  <li>Construct the circuit and test it to ensure it performs the desired function.</li>
</ol>
<h2>Minterms</h2>
<p> Minterms is one approach to writing the boolean expression that represents 
  a truth table, to accomplish step 4 above.</p>
<p>Minterms are made by ANDing the input variables or their complements. Consider 
  the following table with 2 inputs:</p>
<div align="center">
  <table border="1" cellpadding="3" cellspacing="3">
    <tr> 
      <td><div align="center">A</div></td>
      <td><div align="center">B</div></td>
      <td><div align="center">Minterm</div></td>
    </tr>
    <tr> 
      <td>1</td>
      <td>1</td>
      <td>A &#8226; B</td>
    </tr>
    <tr> 
      <td>1</td>
      <td>0</td>
      <td>A &#8226; <img src="images/B_not.PNG" width="16" height="22" align="absmiddle"></td>
    </tr>
    <tr> 
      <td>0</td>
      <td>1</td>
      <td><img src="images/A_not.PNG" width="21" height="19" align="absmiddle">&#8226; 
        B</td>
    </tr>
    <tr> 
      <td>0</td>
      <td>0</td>
      <td><img src="images/A_not.PNG" width="21" height="19" align="absmiddle">&#8226; 
        <img src="images/B_not.PNG" width="16" height="22" align="absmiddle"></td>
    </tr>
  </table>
</div>
<p align="left">When writing the minterm, the variable A or B is used when its 
  value is 1. The complements <img src="images/A_not.PNG" width="21" height="19" align="absbottom"> 
  and <img src="images/B_not.PNG" width="16" height="22" align="absbottom"> 
  are used when the value is 0. Because of this, the minterm is equal to 1. For 
  example, when A = 1 and B = 1, then A&#8226;B = 1. However, when A = 1 and B 
  = 0, then A &#8226; <img src="images/B_not.PNG" width="16" height="22" align="absbottom">= 
  1. </p>
<p align="left">The boolean expression is made by ORing the minterms for rows 
  where the output is desired to be 1. Consider the following example, if we want 
  to create a circuit that would give the following truth table:</p>
<div align="center"> 
  <table border="1" cellpadding="3" cellspacing="3">
    <tr> 
      <td><div align="center">A</div></td>
      <td><div align="center">B</div></td>
      <td>Y</td>
    </tr>
    <tr> 
      <td>1</td>
      <td>1</td>
      <td>1</td>
    </tr>
    <tr> 
      <td>1</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr> 
      <td>0</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr> 
      <td>0</td>
      <td>0</td>
      <td>1</td>
    </tr>
  </table>
</div>
<p align="left">&nbsp;</p>
<p align="left">Then the rows where the output is 1, have the following minterms:</p>

<div align="center">
  <table border="1" cellpadding="3" cellspacing="3">
    <tr> 
      <td><div align="center">A</div></td>
      <td><div align="center">B</div></td>
      <td>Y</td>
      <td><div align="center">Minterm</div></td>
    </tr>
    <tr> 
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>A &#8226; B</td>
    </tr>
    <tr> 
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td><img src="images/A_not.PNG" width="21" height="19" align="absmiddle">&#8226; 
        <img src="images/B_not.PNG" width="16" height="22" align="absmiddle"></td>
    </tr>
  </table>
</div>

<p align="left">We write the minterms on those lines which have an output of 1 
  only. We write the boolean expression by writing the sum (ORs) of these minterms. 
  Therefore, the required expression is:</p>
<p align="left">Y = A&#8226;B + <img src="images/A_not.PNG" width="21" height="19" align="absmiddle">&#8226;<img src="images/B_not.PNG" width="16" height="22" align="absmiddle"></p>
<p align="left">The minterm method always expresses the output as a sum of products 
  of the input variables. It is not necessarily the simplest expression, however, 
  it is a straightforward method that always works.</p>
<p align="left">&nbsp;</p>
<p align="left">Now consider a 3 input situation. The table of minterms is as 
  follows:</p>
  <div align="center">
<table border="1" cellpadding="3" cellspacing="3">
  <tr>
    <td><div align="center">A</div></td>
    <td><div align="center">B</div></td>
    <td><div align="center">C</div></td>
    <td>Minterm</td>
  </tr>
  <tr>
    <td>1</td>
    <td>1</td>
    <td>1</td>
    <td>A &#8226; B &#8226; C</td>
  </tr>
  <tr>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>A &#8226; B &#8226; <img src="images/C_not.PNG" width="21" height="19" align="absmiddle"></td>
  </tr>
  <tr>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>A &#8226; <img src="images/B_not.PNG" width="16" height="22" align="absmiddle">&#8226; 
        C</td>
  </tr>
  <tr>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>A &#8226; <img src="images/B_not.PNG" width="16" height="22" align="absmiddle">&#8226; 
        <img src="images/C_not.PNG" width="21" height="19" align="absmiddle"></td>
  </tr>
  <tr>
      <td>0</td>
      <td>1</td>
      <td>1</td>
      <td><img src="images/A_not.PNG" width="21" height="19" align="absmiddle">&#8226; 
        B &#8226; C</td>
  </tr>
  <tr>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td><img src="images/A_not.PNG" width="21" height="19" align="absmiddle">&#8226; 
        B &#8226; <img src="images/C_not.PNG" width="21" height="19" align="absmiddle"></td>
  </tr>
  <tr>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td><img src="images/A_not.PNG" width="21" height="19" align="absmiddle">&#8226; 
        <img src="images/B_not.PNG" width="16" height="22" align="absmiddle">&#8226; 
        C</td>
  </tr>
  <tr>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td><img src="images/A_not.PNG" width="21" height="19" align="absmiddle">&#8226; 
        <img src="images/B_not.PNG" width="16" height="22" align="absmiddle">&#8226; 
        <img src="images/C_not.PNG" width="21" height="19" align="absmiddle"></td>
  </tr>
</table>
</div>
<p align="left">Consider the following example, if we wanted a circuit that had 
  the following truth table:</p>
<div align="center"> 
  <table border="1" cellpadding="3" cellspacing="3">
    <tr> 
      <td><div align="center">A</div></td>
      <td><div align="center">B</div></td>
      <td><div align="center">C</div></td>
      <td>Y</td>
    </tr>
    <tr> 
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr> 
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr> 
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>1</td>
    </tr>
    <tr> 
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr> 
      <td>0</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr> 
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr> 
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>1</td>
    </tr>
    <tr> 
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
  </table>
</div>
<p align="left">Then the rows that have an output of 1, have the following minterms:</p>
<div align="center"> 
  <table border="1" cellpadding="3" cellspacing="3">
    <tr> 
      <td><div align="center">A</div></td>
      <td><div align="center">B</div></td>
      <td><div align="center">C</div></td>
      <td>Y</td>
      <td>Minterm</td>
    </tr>
    <tr> 
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>A &#8226; B &#8226; <img src="images/C_not.PNG" width="21" height="19" align="absmiddle"></td>
    </tr>
    <tr> 
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>1</td>
      <td>A &#8226; <img src="images/B_not.PNG" width="16" height="22" align="absmiddle">&#8226; 
        C</td>
    </tr>
    <tr> 
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td>0</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>1</td>
      <td><img src="images/A_not.PNG" width="21" height="19" align="absmiddle">&#8226; 
        <img src="images/B_not.PNG" width="16" height="22" align="absmiddle">&#8226; 
        C</td>
    </tr>
    <tr> 
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>&nbsp;</td>
    </tr>
  </table>
</div>
<p align="left">&nbsp;</p>
<p align="left">Once again, we write the minterms on those lines which have an 
  output of 1 only. We write the boolean expression by writing the sum (ORs) of 
  these minterms. Therefore, the required expression is:</p>
<p align="left">Y = A &#8226; B &#8226; <img src="images/C_not.PNG" width="21" height="19" align="absmiddle"> 
  + A &#8226; <img src="images/B_not.PNG" width="16" height="22" align="absmiddle">&#8226; 
  C + <img src="images/A_not.PNG" width="21" height="19" align="absmiddle">&#8226; 
  <img src="images/B_not.PNG" width="16" height="22" align="absmiddle">&#8226; 
  C</p>
<p align="left">Give the expressions represented by the following truth tables:</p>
<p>(a) <br>
  A B Y<br>
  0 0 1<br>
  0 1 1<br>
  1 0 0<br>
  1 1 1<br>
</p>
<p>(b)<br>
  A B C Y<br>
  0 0 0 1<br>
  0 0 1 1<br>
  0 1 0 1<br>
  0 1 1 1<br>
  1 0 0 0<br>
  1 0 1 0<br>
  1 1 0 0<br>
  1 1 1 0<br>
</p>
<p>(c) <br>
  A B C Y<br>
  0 0 0 0<br>
  0 0 1 1<br>
  0 1 0 1<br>
  0 1 1 1<br>
  1 0 0 0<br>
  1 0 1 0<br>
  1 1 0 1<br>
  1 1 1 0</p>
<p>(d) <br>
  A B C Y<br>
  0 0 0 1<br>
  0 0 1 1<br>
  0 1 0 0<br>
  0 1 1 1<br>
  1 0 0 0<br>
  1 0 1 1<br>
  1 1 0 0<br>
  1 1 1 1<br>
</p>
<ol>
  <li>For each expression above, draw the circuit diagram. </li>
  <li>For each expression above, construct your circuit using the logic gate simulator 
    and test it to ensure it gives the required truth table. </li>
  <li>Using the boolean algebra laws, simplify any expressions possible.</li>
</ol>
<h2>Design Problems</h2>
<p>Example Problem:</p>
<p>In some countries, traffic lights have only a red and green signal, they do 
  not have a yellow. To replace the yellow light, the light that was green turns 
  to red while the light pointing at the other road stays red. Therefore, the 
  lights are red in all directions for a short period of time. If the corner has 
  a Walk/Don't Walk signal, it is on only when the green light is showing. Create 
  the logic circuit that can control the Walk/Don't Walk signal for the traffic 
  light in one direction.</p>
<p>Solution: From the design process steps above.</p>
<h3>Step 1:</h3>
<p>Draw a diagram of the intersection with the Walk/Don't Walk signal for one 
  corner. Think about how the red and green lights work together for one traffic 
  light. </p>
<h3>Step 2:</h3>
<p>Let A represent the red light where 1 = on<br>
  Let B represent the green light where 1 = on<br>
  Let Y represent the Walk signal where 1 = on</p>
<h3>Step 3:</h3>
<table border="1" cellpadding="3" cellspacing="3">
  <tr> 
    <td><div align="center">A</div></td>
    <td><div align="center">B</div></td>
    <td>Meaning</td>
    <td>Y</td>
  </tr>
  <tr> 
    <td>0</td>
    <td>0</td>
    <td>caution, don't walk</td>
    <td>0</td>
  </tr>
  <tr> 
    <td>1</td>
    <td>0</td>
    <td>cars stop, don't walk</td>
    <td>0</td>
  </tr>
  <tr> 
    <td>0</td>
    <td>1</td>
    <td>cars go, safe to walk</td>
    <td>1</td>
  </tr>
  <tr> 
    <td>1</td>
    <td>1</td>
    <td>error, don't walk!</td>
    <td>0</td>
  </tr>
</table>
<h3>Step 4:</h3>
<table border="1" cellpadding="3" cellspacing="3">
  <tr> 
    <td><div align="center">A</div></td>
    <td><div align="center">B</div></td>
    <td>Y</td>
    <td><div align="center">Minterm</div></td>
  </tr>
  <tr> 
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td>1</td>
    <td>0</td>
    <td>0</td>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td>0</td>
    <td>1</td>
    <td>1</td>
    <td><img src="images/A_not.PNG" width="21" height="19" align="absmiddle">&#8226; 
      B</td>
  </tr>
  <tr> 
    <td>1</td>
    <td>1</td>
    <td>0</td>
    <td>&nbsp;</td>
  </tr>
</table>
<p>Therefore, the expression is: Y = <img src="images/A_not.PNG" width="21" height="19" align="absmiddle">&#8226; 
  B</p>
<h3>Step 5:</h3>
<p>The simplest form of the expression is: Y = <img src="images/A_not.PNG" width="21" height="19" align="absmiddle">&#8226; 
  B</p>
<h3>Step 6:</h3>
<p><img src="images/traffic_lights_circuit.PNG" width="164" height="72"></p>
<h2>Assignments:</h2>
<ol>
  <li>In an effort to conserve energy, a room should only ever have 1 light on 
    at a time. Design a circuit that will turn on a buzzer when more than one 
    light is turned on at the same time if the room has:</li>
  <ol>
    <li>two lights</li>
    <li>three lights</li>
    <li>four lights</li>
  </ol>
  <p>&nbsp;</p>
  <li>Design a circuit for a School Board of three people that will turn on a 
    green light when a majority vote in favour of a proposition.</li>
	<p>&nbsp;</p>
  <li>Design a circuit for a School Board of three people that will turn on a 
    red light when a proposition fails to pass a vote.</li>
	<p>&nbsp;</p>
  <li>Design a circuit for a School Board of three people that will turn on a 
    blue light when a vote is unanimous, either for or against a proposition.</li>
</ol>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>
�A�Eo��   �K�:      (    y�f�. g*�f�. �   HTTP/1.1 200 OK Content-Length: 15025 Content-Type: text/html Last-Modified: Fri, 07 Dec 2007 23:30:24 GMT Accept-Ranges: bytes ETag: "fc220242939c81:e7bf" Server: Microsoft-IIS/6.0 X-Powered-By: ASP.NET Date: Sun, 23 Apr 2017 07:27:58 GMT        24.137.197.139  P      �R�(�Hh��)$|dS��or�m㬊��Q3uqi�A�Eo��   ֨�f,      